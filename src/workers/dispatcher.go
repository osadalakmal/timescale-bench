package workers

import (
	"fmt"
	"hash/fnv"
	"os"
	"sync"
)

// DispatcherFunc describes a function that takes a given work item
// and a hostname and dispatches the work item to a worker
// determined by the hostname
type DispatcherFunc func(string, WorkItem)

// Dispatcher holds the collection of workers and the function that selects which
// worker should get a request for a given host
type Dispatcher struct {
	selectWorker func(string, []Worker) Worker
	RunTimeQueue RunTimeQueue
	workers      []Worker
}

// NewDispatcher constructs and return a new dispatcher object. Note that this does not
// start the workers. maxQueues is the number of workers that will
// be created. connStr is the connection string used to connect to postgres
func NewDispatcher(maxQueues int, connStr string) Dispatcher {
	var dispatcher Dispatcher
	dispatcher.workers = make([]Worker, 0)
	dispatcher.RunTimeQueue = make(RunTimeQueue, maxQueues)
	dispatcher.selectWorker = func(hostname string, workers []Worker) Worker {
		tempHash := fnv.New32a()
		if _, err := tempHash.Write([]byte(hostname)); err != nil {
			fmt.Fprintf(os.Stderr, "Error creating hash for hostname:%s\n", hostname)
			//TODO This does violate the hostname <-> worker mapping. Is that ok?
			return workers[0]
		}
		return workers[tempHash.Sum32()%uint32(maxQueues)]
	}

	for i := 0; i < maxQueues; i++ {
		workQueue := make(Queue)
		worker := NewWorker(workQueue, connStr)
		dispatcher.workers = append(dispatcher.workers, worker)
	}
	return dispatcher
}

// StartAll starts all the workers
func (d *Dispatcher) StartAll(workFunc WorkerFunc) *sync.WaitGroup {
	wg := sync.WaitGroup{}
	for _, worker := range d.workers {
		wg.Add(1)
		go func(wg *sync.WaitGroup, worker Worker, workFunc WorkerFunc) {
			worker.Start(wg, d.RunTimeQueue, workFunc)
		}(&wg, worker, workFunc)
	}
	return &wg
}

// Dispatch the given work item to a worker determined using host value
func (d *Dispatcher) Dispatch(wg *sync.WaitGroup, host string, item WorkItem) {
	wg.Add(1)
	go func(item WorkItem) {
		worker := d.selectWorker(host, d.workers)
		worker.JobChannel <- item
		wg.Done()
	}(item)
}

// StopAll sends signal stop to all the workers
func (d *Dispatcher) StopAll() {
	for _, worker := range d.workers {
		worker.Stop()
	}
}
