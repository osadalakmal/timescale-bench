package workers

import (
	"fmt"
	"os"
	"path"
	"runtime"
	"testing"
)

func TestParseCsvToSQLString(t *testing.T) {
	_, sourceFile, _, _ := runtime.Caller(0)
	if f, err := os.Open(path.Join(path.Dir(sourceFile), "../../testdata/query_params_test_data.csv")); err != nil {
		t.Errorf("Could not open input data file. err:%v", err)
	} else {
		index := 1
		err = ParseCsvToSQLString(f, false, func(host string, item WorkItem) {
			// The test file contains 1-indexed hostnames in order
			hostFromIdx := "host_" + fmt.Sprintf("%06d", index)
			if hostFromIdx != host {
				t.Errorf("host expected is different from actual host. expected:%s actual:%s\n", hostFromIdx, host)
			}
			index++
		})
		if err != nil {
			t.Errorf("Consuming the csv file failed with error:%v", err)
		}

	}
}
