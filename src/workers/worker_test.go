package workers

import (
	"database/sql"
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestNewWorker(t *testing.T) {
	jobChannel := make(Queue)
	worker := NewWorker(jobChannel, "testConnStr")
	if worker.JobChannel != jobChannel {
		t.Error("Passed in job channel is not stored\n")
	}
	if worker.JobChannel != jobChannel {
		t.Error("Passed in waitGroup is not stored\n")
	}
}

func TestWorker_StartStop(t *testing.T) {
	jobChannel := make(Queue)
	var wg sync.WaitGroup
	wg.Add(1)
	worker := NewWorker(jobChannel, "testConnStr")
	inputItem := WorkItem{
		requestSQL: "test",
	}
	var outputItem *WorkItem
	RunTimeQueue := make(RunTimeQueue, 1)
	go worker.Start(&wg, RunTimeQueue, func(db *sql.DB, workItem WorkItem) (time.Duration, error) {
		outputItem = &workItem
		return 0, nil
	})
	jobChannel <- inputItem
	worker.Stop()
	wg.Wait()
	if inputItem.requestSQL != outputItem.requestSQL {
		t.Errorf("The start method did not execute correctly\n")
	}
}

func TestWorker_AllProcessed(t *testing.T) {
	jobChannel := make(Queue)
	var wg sync.WaitGroup
	wg.Add(1)
	worker := NewWorker(jobChannel, "testConnStr")
	itemCount := 0
	RunTimeQueue := make(RunTimeQueue, 2)
	go worker.Start(&wg, RunTimeQueue, func(db *sql.DB, workItem WorkItem) (time.Duration, error) {
		itemCount++
		return 0, nil
	})
	for i := 0; i < 2; i++ {
		jobChannel <- WorkItem{fmt.Sprintf("%d", i), "testhost"}
	}
	worker.Stop()
	wg.Wait()
	if itemCount != 2 {
		t.Errorf("Not all the items were processed. Input:10 Processed:%d\n", itemCount)
	}
}
