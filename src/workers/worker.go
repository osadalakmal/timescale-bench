package workers

import (
	"database/sql"
	"fmt"
	"github.com/lib/pq"
	"os"
	"sync"
	"time"
)

// Queue is a channel through which we deliver input records to work on
type Queue chan WorkItem

// RuntimeRecord contains fields that are reported from the worker
// after running a query on the database
type RuntimeRecord struct {
	Time   time.Duration
	Failed bool
}

// RunTimeQueue is used to communicate back run time results from
// the worker
type RunTimeQueue chan RuntimeRecord

// Worker represents the worker that executes the job
type Worker struct {
	JobChannel Queue
	db         *sql.DB
	debugStr   string
	quit       chan bool
}

// NewWorker creates a worker struct and initializes the database connection
func NewWorker(jobChannel Queue, connStr string) Worker {
	worker := Worker{
		JobChannel: jobChannel,
		quit:       make(chan bool, 1)}
	var err error
	worker.db, err = sql.Open("postgres", connStr)
	if err, ok := err.(*pq.Error); ok {
		panic(fmt.Sprintf("pq error:%v\n", err.Code.Name()))
	}
	if err != nil {
		panic(fmt.Sprintf("error:%v\n", err))
	}
	return worker
}

// Start method starts the run loop for the worker, listening for a quit channel in
// case we need to stop it
func (w *Worker) Start(wg *sync.WaitGroup, RunTimeQueue RunTimeQueue, workFunc WorkerFunc) {
	for {
		select {
		case workItem := <-w.JobChannel:
			if runTime, err := workFunc(w.db, workItem); err != nil {
				fmt.Fprintf(os.Stderr, "Error while executing job:%v\n", err)
				RunTimeQueue <- RuntimeRecord{0, true}
			} else {
				RunTimeQueue <- RuntimeRecord{runTime, false}
			}
			break

		// TODO Put this before job channel
		case <-w.quit:
			close(w.JobChannel)
			for workItem := range w.JobChannel {
				if runTime, err := workFunc(w.db, workItem); err != nil {
					fmt.Fprintf(os.Stderr, "Error while executing job:%v\n", err)
					RunTimeQueue <- RuntimeRecord{0, true}
				} else {
					RunTimeQueue <- RuntimeRecord{runTime, false}
				}
			}
			w.db.Close()
			wg.Done()
			return
		}
	}
}

// Stop signals the worker to stop listening for work requests.
func (w Worker) Stop() {
	w.quit <- true
}
