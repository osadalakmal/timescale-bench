package workers

import (
	"database/sql"
	"fmt"
	"time"
)

// WorkItem contains data to run a query as required by the assignment
// requestSQL is the query to execute. host is the host whose data we
// are getting out of the database
type WorkItem struct {
	requestSQL string
	host       string
}

type result struct {
	minuteBucket time.Time
	hostname     string
	max          float64
	min          float64
}

// WorkerFunc is the type for workers that process the requests
type WorkerFunc func(*sql.DB, WorkItem) (time.Duration, error)

// RunSQLQuery takes a given SQL query, runs it and records the performance
// characteristics
func RunSQLQuery(db *sql.DB, item WorkItem) (duration time.Duration, err error) {
	start := time.Now()
	res := result{}
	err = db.QueryRow(item.requestSQL).Scan(&res.minuteBucket, &res.hostname, &res.max, &res.min)
	if err != nil {
		fmt.Printf("Failed to query the database error:%v\n", err)
		return time.Duration(0), err
	}
	return time.Since(start), nil
}
