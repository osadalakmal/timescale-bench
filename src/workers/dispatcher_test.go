package workers

import (
	"database/sql"
	"fmt"
	"regexp"
	"strconv"
	"sync"
	"testing"
	"time"
)

func TestCreateQueueMap(t *testing.T) {
	const maxQueues = 5
	dispatcher := NewDispatcher(maxQueues, "testconn")
	if len(dispatcher.workers) != maxQueues {
		t.Errorf("No of queues not equal to max queues. No Queues:%d, maxQueues:%d",
			len(dispatcher.workers), maxQueues)
	}
	queueForHost1 := dispatcher.selectWorker("myhost", dispatcher.workers)
	queueForHost2 := dispatcher.selectWorker("myhost", dispatcher.workers)
	if queueForHost1.JobChannel != queueForHost2.JobChannel {
		t.Errorf("The selected bucket function cannot consistently select the same bucket for same host")
	}
}

func TestDispatch(t *testing.T) {
	const maxQueues = 2
	const workItemCount = 100
	var data [workItemCount]bool
	var runTimes []time.Duration
	dispatcher := NewDispatcher(maxQueues, "testconn")
	workerWg := dispatcher.StartAll(func(db *sql.DB, item WorkItem) (time.Duration, error) {
		i, err := strconv.Atoi(regexp.MustCompile("host_(\\d+)").FindStringSubmatch(item.host)[1])
		if err == nil {
			data[i] = true
		}
		return 0, nil
	})

	runtimeWg := sync.WaitGroup{}
	runtimeWg.Add(1)
	go func(RunTimeQueue RunTimeQueue, runTimes *[]time.Duration, runtimeWg *sync.WaitGroup) {
		for runtimeRecords := range RunTimeQueue {
			*runTimes = append(*runTimes, runtimeRecords.Time)
		}
		runtimeWg.Done()
	}(dispatcher.RunTimeQueue, &runTimes, &runtimeWg)

	senderWg := sync.WaitGroup{}
	for i := 0; i < workItemCount; i++ {
		host := fmt.Sprintf("host_%d", i)
		dispatcher.Dispatch(&senderWg, host, WorkItem{"testsql", host})
	}
	senderWg.Wait()
	dispatcher.StopAll()
	time.Sleep(time.Second * 5)
	workerWg.Wait()
	close(dispatcher.RunTimeQueue)
	runtimeWg.Wait()

	for i := 0; i < workItemCount; i++ {
		if !data[i] {
			t.Fatalf("Item %d has not been processed\n", i)
		}
	}
	if len(runTimes) != workItemCount {
		t.Fatalf("Not all run times have been recorded. Expected:%d Actual:%d\n", workItemCount, len(runTimes))
	}
}
