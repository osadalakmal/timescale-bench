package workers

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
)

// RequestSQL is a templated sql command that obtains the data specified in
// the assignment spec
const RequestSQL = "SELECT time_bucket('1 minutes', ts) AS one_min, host, max(usage) " +
	", min(usage) FROM cpu_usage WHERE host = '%s' AND ts BETWEEN " +
	"to_timestamp('%s', 'YYYY-MM-DD HH24:MI:SS') AND " +
	"to_timestamp('%s', 'YYYY-MM-DD HH24:MI:SS')  GROUP BY one_min, host " +
	"ORDER BY one_min"

// ParseCsvToSQLString parses the input file and generate a stream of SQL
// string corresponding to the lines in the file and call
// the consumerFunc function with the resulting strings
// TODO input sanitation?
func ParseCsvToSQLString(input io.Reader, skipHeader bool, dispatcherFunc DispatcherFunc) error {
	csvReader := csv.NewReader(input)
	badRecordCount := 0
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			return nil
		} else if err != nil {
			panic(fmt.Sprintf("Error while reading input csv. err:%v", err))
		}
		if len(record) != 3 {
			badRecordCount++
		}
		if skipHeader {
			skipHeader = false
			continue
		}
		dispatcherFunc(record[0], WorkItem{
			fmt.Sprintf(RequestSQL, record[0], record[1], record[2]),
			record[0]})
	}
	if badRecordCount != 0 {
		fmt.Fprintf(os.Stderr, "Found %d bad records in the input csv file\n", badRecordCount)
		return errors.New("Bad records found in the input file")
	}
	return nil
}
