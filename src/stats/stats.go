// Package stats contains code for calculating and printing out
// statistical properties for runtime data captured by the tool
// Stolen with love from tsbs and improved upon
package stats

import (
	"fmt"
	"math"
	"sort"
	"workers"
)

// StatGroup collects simple streaming statistics.
type StatGroup struct {
	min    float64
	max    float64
	mean   float64
	sum    float64
	values []float64

	// used for stddev calculations
	m      float64
	s      float64
	stdDev float64

	count       int64
	failedCount int64
}

// NewStatGroup returns a new StatGroup with an initial size
func NewStatGroup(size uint64) *StatGroup {
	return &StatGroup{
		values: make([]float64, size),
		count:  0,
	}
}

// median returns the median value of the StatGroup
func (s *StatGroup) median() float64 {
	sort.Float64s(s.values[:s.count])
	if s.count == 0 {
		return 0
	} else if s.count%2 == 0 {
		idx := s.count / 2
		return (s.values[idx] + s.values[idx-1]) / 2.0
	} else {
		return s.values[s.count/2]
	}
}

func (s *StatGroup) madv() float64 {
	madSum := 0.0
	median := s.median()
	for _, value := range s.values {
		madSum += math.Abs(value - median)
	}
	return madSum / float64(s.count)
}

// Push updates a StatGroup with a new value.
func (s *StatGroup) Push(runTimeRecrod workers.RuntimeRecord) {

	if runTimeRecrod.Failed {
		s.failedCount++
		return
	}
	// We convert ns to ms
	r := float64(runTimeRecrod.Time)/1000000
	if s.count == 0 {
		s.min = r
		s.max = r
		s.mean = r
		s.count = 1
		s.sum = r

		s.m = r
		s.s = 0.0
		s.stdDev = 0.0
		if len(s.values) > 0 {
			s.values[0] = r
		} else {
			s.values = append(s.values, r)
		}
		return
	}

	if r < s.min {
		s.min = r
	}
	if r > s.max {
		s.max = r
	}

	s.sum += r

	// constant-space mean update:
	sum := s.mean*float64(s.count) + r
	s.mean = sum / float64(s.count+1)
	if int(s.count) == len(s.values) {
		s.values = append(s.values, r)
	} else {
		s.values[s.count] = r
	}

	s.count++

	oldM := s.m
	s.m += (r - oldM) / float64(s.count)
	s.s += (r - oldM) * (r - s.m)
	s.stdDev = math.Sqrt(s.s / (float64(s.count) - 1.0))
}

// string makes a simple description of a StatGroup.
func (s *StatGroup) String() string {
	return fmt.Sprintf("min: %8.2fms, med: %8.2fms, mean: %8.2fms, max: %7.2fms, stddev: %8.2fms, madv: %8.2fms, sum: %5.1fsec, count: %d, failed count: %d", s.min, s.median(), s.mean, s.max, s.stdDev, s.madv(), s.sum/1e3, s.count, s.failedCount)
}
