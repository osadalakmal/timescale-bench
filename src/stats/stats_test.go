package stats

import (
	"testing"
	"time"
	"workers"
)

func TestStatsGroupMedian(t *testing.T) {
	cases := []struct {
		len  uint64
		want float64
	}{
		{
			len:  0,
			want: 0.0,
		},
		{
			len:  1,
			want: 1.0,
		},
		{
			len:  2,
			want: 2.0,
		},
		{
			len:  4,
			want: 4.0,
		},
		{
			len:  5,
			want: 5.0,
		},
		{
			len:  1000,
			want: 1000,
		},
	}

	for _, c := range cases {
		sg := NewStatGroup(c.len)
		for i := uint64(0); i < c.len; i++ {
			sg.Push(workers.RuntimeRecord{time.Duration((1+i*2)*1000000),false})
		}
		if got := sg.median(); c.want != got {
			t.Errorf("got: %v want: %v\n", got, c.want)
		}
	}
}

func TestStatsGroupMadv(t *testing.T) {
	cases := []struct {
		len  uint64
		want float64
	}{
		{
			len:  2,
			want: 0.5,
		},
		{
			len:  4,
			want: 1.0,
		},
		{
			len:  5,
			want: 1.2,
		},
		{
			len:  10,
			want: 2.5,
		},
	}

	for _, c := range cases {
		sg := NewStatGroup(c.len)
		for i := uint64(0); i < c.len; i++ {
			sg.Push(workers.RuntimeRecord{time.Duration((1+i)*1000000),false})
		}
		if got := sg.madv(); c.want != got {
			t.Errorf("got: %v want: %v\n", got, c.want)
		}
	}
}

func TestStatsGroupMedian0InitialSize(t *testing.T) {
	cases := []struct {
		len  uint64
		want float64
	}{
		{
			len:  0,
			want: 0.0,
		},
		{
			len:  1,
			want: 1.0,
		},
		{
			len:  2,
			want: 2.0,
		},
		{
			len:  4,
			want: 4.0,
		},
		{
			len:  5,
			want: 5.0,
		},
		{
			len:  1000,
			want: 1000,
		},
	}

	for _, c := range cases {
		sg := NewStatGroup(0)
		for i := uint64(0); i < c.len; i++ {
			sg.Push(workers.RuntimeRecord{time.Duration((1 + i*2)*1000000), false})
		}
		if got := sg.median(); c.want != got {
			t.Errorf("got: %v want: %v\n", got, c.want)
		}
	}
}

func TestStatsGroupPush(t *testing.T) {
	cases := []struct {
		desc      string
		vals      []int64
		wantMin   float64
		wantMax   float64
		wantMean  float64
		wantCount int64
		wantSum   float64
	}{
		{
			desc:      "ordered smallest to largest",
			vals:      []int64{0.0, 1.0, 2.0},
			wantMin:   0.0,
			wantMax:   2.0,
			wantMean:  1.0,
			wantCount: 3,
			wantSum:   3.0,
		},
		{
			desc:      "ordered largest to smallest",
			vals:      []int64{2.0, 1.0, 0.0},
			wantMin:   0.0,
			wantMax:   2.0,
			wantMean:  1.0,
			wantCount: 3,
			wantSum:   3.0,
		},
		{
			desc:      "out of order",
			vals:      []int64{17.0, 10.0, 12.0},
			wantMin:   10.0,
			wantMax:   17.0,
			wantMean:  13.0,
			wantCount: 3,
			wantSum:   39.0,
		},
	}

	for _, c := range cases {
		sg := NewStatGroup(0)
		for _, val := range c.vals {
			sg.Push(workers.RuntimeRecord{time.Duration(val*1000000),false})
		}
		if got := sg.min; got != c.wantMin {
			t.Errorf("%s: incorrect min: got %f want %f", c.desc, got, c.wantMin)
		}
		if got := sg.max; got != c.wantMax {
			t.Errorf("%s: incorrect max: got %f want %f", c.desc, got, c.wantMin)
		}
		if got := sg.mean; got != c.wantMean {
			t.Errorf("%s: incorrect mean: got %f want %f", c.desc, got, c.wantMin)
		}
		if got := sg.count; got != c.wantCount {
			t.Errorf("%s: incorrect count: got %d want %d", c.desc, got, c.wantCount)
		}
		if got := sg.sum; got != c.wantSum {
			t.Errorf("%s: incorrect sum: got %f want %f", c.desc, got, c.wantMin)
		}
	}
}
