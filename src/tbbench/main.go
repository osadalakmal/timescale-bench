package main

import (
	"config"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"runtime/trace"
	"sort"
	"stats"
	"sync"
	"time"
	"workers"
)

func main() {
	statsRec := stats.NewStatGroup(1)
	f, err := os.Create(".cputrace")
	if err != nil {
		log.Fatal(err)
	}
	trace.Start(f)
	defer trace.Stop()

	options := config.ParseCmdlineOptions()
	dispatcher := workers.NewDispatcher(options.MaxWorkers, config.GetPostgresConnectString(options))
	workerWg := dispatcher.StartAll(workers.RunSQLQuery)
	doneRuntimeGathering := startRuntimeReadingGatherer(dispatcher, statsRec)
	generateWorkItemsFromInput(options, dispatcher)

	dispatcher.StopAll()
	workerWg.Wait()
	close(dispatcher.RunTimeQueue)
	<-doneRuntimeGathering

	fmt.Println(statsRec.String())
}

func generateWorkItemsFromInput(options config.CmdlineOptions, dispatcher workers.Dispatcher) {
	senderWg := sync.WaitGroup{}
	err := workers.ParseCsvToSQLString(openInputFile(options), options.SkipHeader, func(host string, item workers.WorkItem) {
		dispatcher.Dispatch(&senderWg, host, item)
	})
	if err != nil {
		panic(fmt.Sprintf("Error while reading the input file. err:%v\n", err))
	}
	senderWg.Wait()
}

func openInputFile(options config.CmdlineOptions) io.Reader {
	var input io.Reader
	var err error
	if input, err = os.Open(options.InputFile); err != nil {
		panic("Could not open input file")
	}
	return input
}

func median(input []time.Duration) (median time.Duration) {
	l := len(input)
	if l%2 == 0 {
		return (input[(l/2)-1] + input[(l/2)+1]) / 2
	}
	return input[l/2]
}

func countStats(runTimes []time.Duration) {
	var total time.Duration
	var min time.Duration = math.MaxInt64
	var max time.Duration
	sort.Slice(runTimes, func(a int, b int) bool { return runTimes[a] < runTimes[b] })
	for _, timeForQuery := range runTimes {
		total = total + timeForQuery
		if timeForQuery < min {
			min = timeForQuery
		}
		if timeForQuery > max {
			max = timeForQuery
		}
	}
	median := median(runTimes)
	fmt.Printf("no of queries:%d\ntotal:%d ms\nmin:%f us\nmax:%f us\nmean:%f us\nmedian:%f us\n",
		len(runTimes), total/1000000, float64(min)/1000, float64(max)/1000,
		float64(total)/float64(len(runTimes)*1000), float64(median)/1000)
}

func startRuntimeReadingGatherer(dispatcher workers.Dispatcher, statsRec *stats.StatGroup) chan bool {
	done := make(chan bool, 1)
	go func(runTimeQueue workers.RunTimeQueue, statsRec *stats.StatGroup, done chan bool) {
		for runtimeRecord := range runTimeQueue {
			statsRec.Push(runtimeRecord)
		}
		done <- true
	}(dispatcher.RunTimeQueue, statsRec, done)
	return done
}
