package config

import (
	"flag"
	"fmt"
)

// CmdlineOptions is a structure for storing options passed in
// at the command line
type CmdlineOptions struct {
	PostgresUser       string
	PostgresHost       string
	DatabaseName       string
	MiscConnectOptions string
	InputFile          string
	MaxWorkers         int
	SkipHeader         bool
}

// ParseCmdlineOptions parses command line options and fills a CmdLineOptions struct
// with the resulting data
func ParseCmdlineOptions() CmdlineOptions {
	var options CmdlineOptions
	flag.StringVar(&options.PostgresUser, "pguser", "postgres", "Postgres user to connect as")
	flag.StringVar(&options.PostgresHost, "pghost", "localhost", "Postgres server host to connect to")
	flag.StringVar(&options.DatabaseName, "dbname", "", "Postgres database to use")
	flag.StringVar(&options.MiscConnectOptions, "miscOptions", "-sslmode=disable", "Additional postgres connection options")
	flag.StringVar(&options.InputFile, "infile", "", "File in csv format which contains the parameters for the querieslibpq te")
	flag.IntVar(&options.MaxWorkers, "maxworkers", 4, "Maximum number of workers ")
	flag.BoolVar(&options.SkipHeader, "skipheader", false, "Set true to skip the header row in the CSV file")
	flag.Parse()
	return options
}

// GetPostgresConnectString creates a psql connection
// string based on passed in command line options
func GetPostgresConnectString(options CmdlineOptions) string {
	return fmt.Sprintf("host=%s user=%s dbname=%s %s",
		options.PostgresHost, options.PostgresUser,
		options.DatabaseName, options.MiscConnectOptions)
}
