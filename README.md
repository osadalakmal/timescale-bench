## Timescale Benc (tbbench)

### Overview

This application benchmarks query performance of a postgresql database with timescaledb
extension installed. It uses a configurable number of clients. The clients query the database concurrently. Currently it uses the most widely used native postgres go client [pq](https://github.com/lib/pq).

### The details of the benchmark

This tool benchmarks concurrent query performance under load using a configurable number of native go clients executing queries in parallel. The postgresql database being benchmarked should have the following schema

### Setup for using the tool

A database should be created with the following schema. Also timescaledb extension should be crated and a hyper table created over the `ts` column

| Column | Type | Collation | Nullable | Default | Storage  | Stats target | Description|
|---|---|---|---|---|---|---|---|
| ts     | timestamp with time zone |         | not null |         | plain    |   | |
| host   | text                     |         |          |         | extended |   | |
| usage  | double precision         |         |          |         | plain    |   | |

A script - `testdata/cpu_usage.sql` - has been provided that will create a database named `homework` and then create the `cpu_usage` table with the required schema and setup the hypertable. You can change the database name and table name as required.

### Usage
```
Usage of ./tbbench:
  -dbname string
    	Postgres database to use
  -infile string
    	File in csv format which contains the parameters for the querieslibpq te
  -maxworkers int
    	Maximum number of workers  (default 4)
  -miscOptions string
    	Additional postgres connection options (default "-sslmode=disable")
  -pghost string
    	Postgres server host to connect to (default "localhost")
  -pguser string
    	Postgres user to connect as (default "postgres")
  -skipheader
    	Set true to skip the header row in the CSV file
```

#### Input format

The input file is a CSV file with three columns in the following order

1. hostname : The hostname whose records will be queried
2. start time stamp
3. end time stamp : The benchmark tool will query the records between these two timestamps

The full query takes the records between given timestamps for the given hostname, puts them in to 1 min buckets and takes the minimum and maximum value for each bucket and returns the data. There is no ordering imposed on the data.

#### Output format

The output contains various statistics caluclated over the times taken to run the queries
generated using the input data mentioned above

1. of queries processed
2. total processing time across all queries
2. the minimum query time (for a single query)
3. the median query time
4. the average query time
5. and the maximum query time
6. standard deviation of query time
7. median absolute deviation (MAD) of query time
8. number of failed queries

### Error handling

* Any errors found in the input file will be skipped over and a count of the lines with incorrect data will be printed to the stderr.
* Any failed queries will not be counted towards the final statistics and will be reported as a seperate count.

### Design Document

[Design Document](design.md)

### Development

The code can be built with standard go tools with the command

```
make
```

The unit tests can be run with

```
make test
```

System test can be run with
```
make system-test
```

System tests assumes you have a database named homework with a table as described above and a user named postgres with
a password given by env variable `POSTGRES_USER_PW`. You have to populate it with the dataset in `testdata/cpu_usage.csv`.
The test asserts on the format of the output and makes sure the program exits cleanly while outputting all the required
statistics.

Note that the above command will also fetch the dependancies of the tool

### Known Issues

Currently the application spawns a goroutine for every line encuntered in the CSV file.
Although goroutine's are lightweight this is not feasible for very large input files.
This can be improved upon by batching the input into a size larger than 1 so we create
a sustainable number of goroutines.
