## Design of the timescale-bench system

The design consists of three main packages

* main application package - tbbench
  * This contains the main application code that setups up the
    other components and wires them up. It also issues commands
    to them and gathers the output and displays it.
* configuration handling package - config
  * This contains the configuration code for pasing the command
    line options.
* workers package
  * This contains all the worker related functionality. Bulk of the
    logic is in here. It is responsible for reading the input data,
    processing it and coming up with the raw output data

### workers package

This package consists of the following components

* input_parser
  * This handles the input. It reads the input, creates a SQL query to
    be run from the input record and hands it to the dispatcher to be
    dispatched to the workers.
* dispatcher
  * This holds references to all the workers. It is responsible for
    creating the workers, starting them, stopping them and dispatching
    work items to them. The mapping function that maps a given work ite,
    to a worker is also stored here but that is passed in to the component
* worker
  * This contains the logic for the worker processing the data. It uses two
    channels to get input data. On one it gets the actual work items to work on
    and on the other it gets commands to execute. For now the only command is
    stop which stops the worker It also holds the resources required for the
    worker which currently is a database connection
* work_func
  * This contains the actual logic for running the queries and handling the
    errors from it. It also reports back the run time for the query


### Simplified data flow diagram

```
(input parser) -> (dispatcher go routines) -> |worker job channel| -> (worker go routine) -> |runtime channel| -> (runtime gatherer go routine)
```

In this diagram items in parantheses -`()`- denotes a piece of logic that transportms a given input in to a different form of output.
They all execute independently in their own go routine.
Items in `||` are channels that carry the data to other logic executing independantly of other execution contexts.