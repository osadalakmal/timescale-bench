export PKG_LIST=$(shell ls -1 $$(pwd)/src | grep -v testdata | grep -v github | grep -v golang | tr '\n' ' ')

.PHONY: all dep build clean test coverage coverhtml lint

all: build


lint:
	GOPATH=$$(pwd):$$GOPATH golint -set_exit_status ${PKG_LIST}

test:
	GOPATH=$$(pwd):$$GOPATH go test -short ${PKG_LIST}

race: dep
	GOPATH=$$(pwd):$$GOPATH go test -race -short ${PKG_LIST}

msan: dep
	GOPATH=$$(pwd):$$GOPATH go test -msan -short ${PKG_LIST}

coverage: dep
	echo ${PKG_LIST} | tr ' ' '\n' | \
        while read -r package ; do \
            GOPATH=$$(pwd):$$GOPATH xargs -n1 go test -coverprofile .testCoverage-$${package}.txt -v; \
        done
	# TODO Setup codecov

dep:
	GOPATH=$$(pwd):$$GOPATH go get -v -d github.com/lib/pq

build: dep
	GOPATH=$$(pwd):$$GOPATH go build -v tbbench

clean:
	git clean -xdf

system-test: build
	@./tbbench -dbname homework -infile testdata/query_params.csv -maxworkers=3 -miscOptions \
	"password=i$${POSTGRES_USER_PW} sslmode=disable" -pghost localhost -pguser postgres | \
	egrep "min:[[:space:]]+[.0-9]+ms, med:[[:space:]]+[.0-9]+ms, mean:[[:space:]]+[.0-9]+ms, max:[[:space:]]+[.0-9]+ms, stddev:[[:space:]]+[.0-9]+ms, madv:[[:space:]]+[.0-9]+ms, sum:[[:space:]]+[.0-9]+sec, count:[[:space:]]+[.0-9]+, failed count:[[:space:]]+[.0-9]+" > /dev/null

help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
